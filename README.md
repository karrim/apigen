# Apigen

## Installing

Add `apigen` to your `pubspec.yaml`:
```
dependencies:
  apigen: any
```

## Examples

See `apigen_test.dart` in `test` directory.

## Classes

- `Var`
    - `ConstVar`
    - `FinalVar`
- `Func`
    - `NamedArgument`
    - `PositionalArgument`
- `Class`
- `Lib`
    - `Import`
        - `Hides`
        - `Shows`