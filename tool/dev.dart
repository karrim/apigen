library tool.dev;

import 'package:dart_dev/dart_dev.dart' show dev, config;

main(List<String> args) async {
  config.analyze.entryPoints = ['lib/', 'test/', 'tool/'];
  config.copyLicense.directories = ['lib/'];
  // config.coverage
  // config.docs
  // config.examples
  config.format.directories = ['lib/', 'test/', 'tool/'];
  config.test
    ..unitTests = ['test/'];

  await dev(args);
}
