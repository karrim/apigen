library apigen.test.apigen_test;

import 'package:apigen/apigen.dart';
import 'package:test/test.dart';

void main() {
  test('Class', () {
    var c = new Class('Asd');
    expect(c.toString(), equals('class Asd {}'));

    c = new Class('Asd', isAbstract: true);
    expect(c.toString(), equals('abstract class Asd {}'));

    var f = new Func('');
    c = new Class('Asd', constructors: [f]);
    expect(c.toString(), equals('''
class Asd {

  Asd() {}
}'''));

    var f1 = new Func('named');
    c = new Class('Asd', constructors: [f1]);
    expect(c.toString(), equals('''
class Asd {

  Asd.named() {}
}'''));

    c = new Class('Asd', constructors: [f, f1]);
    expect(c.toString(), equals('''
class Asd {

  Asd() {}

  Asd.named() {}
}'''));

    c = new Class('Asd', functions: [f1]);
    expect(c.toString(), equals('''
class Asd {

  named() {}
}'''));

    c = new Class('Asd', staticFunctions: [f1]);
    expect(c.toString(), equals('''
class Asd {

  static named() {}
}'''));

    var v = new Var('asd');
    c = new Class('Asd', variables: [v]);
    expect(c.toString(), equals('''
class Asd {

  var asd = null;
}'''));

    c = new Class('Asd', staticVariables: [v]);
    expect(c.toString(), equals('''
class Asd {

  static var asd = null;
}'''));

    var c1 = new Class('Sdf');
    c = new Class('Asd', parameterizedTypes: [c1.type]);
    expect(c.toString(), equals('class Asd<Sdf> {}'));
    var c2 = new Class('Dfg', parameterizedTypes: [c.type, c1.type]);
    expect(c2.toString(), equals('class Dfg<Asd<Sdf>, Sdf> {}'));
  });
  test('ApiType', () {
    expect(ApiType.DYNAMIC, equals(ApiType.DYNAMIC));
    expect(ApiType.BOOL, isNot(equals(ApiType.DYNAMIC)));

    expect(new ApiType('asd'), equals(new ApiType('asd')));
    expect(new ApiType('asd'), isNot(equals(new ApiType('sdf'))));

    expect(new ApiType('asd', [new ApiType('sdf')]),
        equals(new ApiType('asd', [new ApiType('sdf')])));
    expect(new ApiType('asd', [new ApiType('sdf')]),
        isNot(equals(new ApiType('asd', [new ApiType('dfg')]))));

    var t = new ApiType('Asd');
    expect(t.toString(), equals('Asd'));
    var t1 = new ApiType('Sdf', [t]);
    expect(t1.toString(), equals('Sdf<Asd>'));
    var t2 = new ApiType('Dfg', [t, t1]);
    expect(t2.toString(), equals('Dfg<Asd, Sdf<Asd>>'));
  });

  group('ApiVariable', () {
    test('ConstVar', () {
      var v = new ConstVar('var1');
      expect(v.toString(), equals('const var1 = null'));

      v = new ConstVar('var2', type: ApiType.BOOL);
      expect(v.toString(), equals('const bool var2 = null'));

      v = new ConstVar('var3', value: '15');
      expect(v.toString(), equals('const var3 = 15'));
    });
  });

  test('Func', () {
    var f = new Func('asd');
    expect(f.name, equals('asd'));
    expect(f.type, equals(ApiType.DYNAMIC));
    expect(f.toString(), equals('asd() {}'));

    f = new Func('asd', type: ApiType.DURATION);
    expect(f.name, equals('asd'));
    expect(f.type, equals(ApiType.DURATION));
    expect(f.toString(), equals('Duration asd() {}'));

    var a = new Arg('a');
    var b = new NamedArg('b', type: ApiType.INT, value: '88');
    var c = new PositionalArg('c', type: ApiType.DOUBLE);

    f = new Func('asd',
        type: ApiType.DURATION,
        arguments: [a],
        optionalArguments: new NamedArgumentsDeclaration([b]));
    expect(f.name, equals('asd'));
    expect(f.type, equals(ApiType.DURATION));
    expect(f.toString(), equals('Duration asd(a, {int b: 88}) {}'));

    f = new Func('asd',
        type: ApiType.DURATION,
        arguments: [a],
        optionalArguments: new PositionalArgumentsDeclaration([c]));
    expect(f.name, equals('asd'));
    expect(f.type, equals(ApiType.DURATION));
    expect(f.toString(), equals('Duration asd(a, [double c = null]) {}'));
  });
  group('Lib', () {
    test('As', () {
      var a = new As('asdf');
      expect(a.toString(), equals('as asdf'));

      a = new As('asdf', deferred: true);
      expect(a.toString(), equals('deferred as asdf'));
    });
    test('Hides', () {
      var h = new Hides(['a']);
      expect(h.toString(), equals('hide a'));

      h = new Hides(['a', 'b']);
      expect(h.toString(), equals('hide a, b'));
    });
    test('Lib', () {
      var l = new Lib('asd');
      expect(l.toString(), equals('library asd;\n'));

      var i = new Import('asdf.dart');
      var i1 = new Import('fgh.dart');
      l = new Lib('asd', imports: [i, i1]);
      expect(l.toString(), equals('''
library asd;

import 'asdf.dart';
import 'fgh.dart';'''));

      var es = ['abc.dart', 'jkl.dart'];
      l = new Lib('asd', exports: es);
      expect(l.toString(), equals('''
library asd;

export 'abc.dart';
export 'jkl.dart';'''));

      var c = new Class('Asd');
      var c1 = new Class('Sdf');
      l = new Lib('asd', classes: [c, c1], exports: es);
      expect(l.toString(), equals('''
library asd;

export 'abc.dart';
export 'jkl.dart';
class Asd {}
class Sdf {}'''));

      var f = new Func('asd');
      l = new Lib('asd', classes: [c, c1], exports: es, functions: [f]);
      expect(l.toString(), equals('''
library asd;

export 'abc.dart';
export 'jkl.dart';
asd() {}
class Asd {}
class Sdf {}'''));

      var v = new Var('asd');
      l = new Lib('asd', classes: [c, c1], exports: es, functions: [f], variables: [v]);
      expect(l.toString(), equals('''
library asd;

export 'abc.dart';
export 'jkl.dart';
var asd = null;
asd() {}
class Asd {}
class Sdf {}'''));
    });
    test('Shows', () {
      var s = new Shows(['a']);
      expect(s.toString(), equals('show a'));

      s = new Shows(['a', 'b']);
      expect(s.toString(), equals('show a, b'));
    });
    test('Import', () {
      var i = new Import('asd.dart');
      expect(i.toString(), equals("import 'asd.dart'"));

      var as = new As('asdf');
      i = new Import('asd.dart', as: as);
      expect(i.toString(), equals("import 'asd.dart' as asdf"));

      var s = new Shows(['a', 'b', 'c']);
      as = new As('asdf', deferred: true);
      i = new Import('asd.dart', as: as, properties: s);
      expect(i.toString(), equals("import 'asd.dart' deferred as asdf show a, b, c"));
    });
  });

  test('NamedArg', () {
    var v = new NamedArg('var1');
    expect(v.toString(), equals('var1: null'));

    v = new NamedArg('var2', type: ApiType.BOOL);
    expect(v.toString(), equals('bool var2: null'));

    v = new NamedArg('var3', value: '15');
    expect(v.toString(), equals('var3: 15'));
  });

  test('FinalVar', () {
    var v = new FinalVar('var1');
    expect(v.toString(), equals('final var1 = null'));

    v = new FinalVar('var2', type: ApiType.BOOL);
    expect(v.toString(), equals('final bool var2 = null'));

    v = new FinalVar('var3', value: '15');
    expect(v.toString(), equals('final var3 = 15'));
  });
}
