# Changelog

## 0.0.1

- Initial version, created by Stagehand

## 0.0.2

- Implemented `ApiVariable`

## 0.2.0

- Implemented `ApiFunction`

## 0.2.0

- Renamed Function to Func, Argument to Arg and Variable to Var

## 0.2.1

- Renamed files and implemented `ApiClass`

## 0.2.2

- Implemented `Lib`

## 0.3.0

- Renamed `ApiClass` to `Class`
- Update `README.md`

## 0.4.0

- Renamed *Declaration classes
