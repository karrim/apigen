// Copyright (c) 2016, Era Productions. All rights reserved.

library apigen.src.api_library;

import 'base.dart' show ApiElement;
import 'class.dart';
import 'func.dart';
import 'var.dart';

class As {
  final bool deferred;
  final String name;

  const As(this.name, {this.deferred: false});

  String toString() {
    if (deferred) {
      return 'deferred as $name';
    }
    return 'as $name';
  }
}

class Hides extends ImportProperties {
  const Hides(List<String> imports) : super(imports);

  String toString() => 'hide ${names.join(', ')}';
}

class Import {
  final As as;
  final bool deferred;
  final String path;
  final ImportProperties properties;

  const Import(this.path, {this.as, this.deferred: false, this.properties});

  String toString() {
    final buf = new StringBuffer("import '$path'");
    if (null != as) {
      buf.write(' $as');
    }
    if (null != properties && properties.names.isNotEmpty) {
      buf.write(' $properties');
    }
    return buf.toString();
  }
}

abstract class ImportProperties {
  final List<String> names;

  const ImportProperties(this.names);

  String toString();
}

class Lib extends ApiElement {
  final List<Class> classes;
  final List<String> exports;
  final List<Func> functions;
  final List<Import> imports;
  final List<Var> variables;

  const Lib(String name,
      {this.classes: const <Class>[],
      this.exports: const <String>[],
      this.functions: const <Func>[],
      this.imports: const <Import>[],
      this.variables: const <Var>[]})
      : super(name);

  String toString() {
    final buf = new StringBuffer('library $name;\n');
    for (var e in exports) {
      buf.write("\nexport '$e';");
    }
    for (var i in imports) {
      buf.write('\n$i;');
    }
    for (var v in variables) {
      buf.write('\n$v;');
    }

    for (var f in functions) {
      buf.write('\n$f');
    }
    for (var c in classes) {
      buf.write('\n$c');
    }
    return buf.toString();
  }
}

class Shows extends ImportProperties {
  const Shows(List<String> shows) : super(shows);

  String toString() => 'show ${names.join(', ')}';
}
