// Copyright (c) 2016, Era Productions. All rights reserved.

library apigen.src.base;

import 'package:collection/collection.dart';

class _ApiElement extends ApiElement {

  _ApiElement(String name) : super(name);
}

abstract class ApiElement {
  final String name;

  const ApiElement(this.name);

  factory ApiElement.internal(String name) => new _ApiElement(name);

  String toString();
}

class TypedApiElement extends ApiElement {
  final ApiType type;

  const TypedApiElement(String name, this.type) : super(name);
}

class ApiType extends ApiElement {
  final ListEquality<ApiType> _equality = const ListEquality<ApiType>();
  final List<ApiType> parameterizedTypes;

  const ApiType(String name, [this.parameterizedTypes = const <ApiType>[]])
  : super(name);


  String toString() {
    if (parameterizedTypes.isEmpty) {
      return name;
    }
    final buf = new StringBuffer('$name<');
    for (var type in parameterizedTypes) {
      buf.write('$type');
      if (parameterizedTypes.last != type) {
        buf.write(', ');
      }
    }
    buf.write('>');
    return buf.toString();
  }

  bool operator ==(ApiType t) =>
      name == t.name && _equality.equals(parameterizedTypes, t.parameterizedTypes);

  static const ApiType BOOL = const ApiType('bool');
  static const ApiType DATE_TIME = const ApiType('DateTime');
  static const ApiType DOUBLE = const ApiType('double');
  static const ApiType DURATION = const ApiType('Duration');
  static const ApiType DYNAMIC = const ApiType('dynamic');
  static const ApiType INT = const ApiType('int');
  static const ApiType NUM = const ApiType('num');
  static const ApiType OBJECT = const ApiType('Object');
  static const ApiType STRING = const ApiType('String');
}
