// Copyright (c) 2016, Era Productions. All rights reserved.

library apigen.src.variable;

import 'base.dart';

abstract class AbstractVar extends TypedApiElement {
  final String value;
  const AbstractVar(String name,
      {ApiType type: ApiType.DYNAMIC, this.value: 'null'})
      : super(name, type);

  bool operator ==(AbstractVar v) => super == v && value == v.value;

  String toString();
}

class Arg extends Var {
  Arg(String name, {ApiType type: ApiType.DYNAMIC})
      : super(name, type: type);

  String toString() {
    if (ApiType.DYNAMIC == type) {
      return name;
    }
    return '$type $name';
  }
}

class ConstVar extends AbstractVar {
  ConstVar(String name,
      {ApiType type: ApiType.DYNAMIC, String value: 'null'})
      : super(name, type: type, value: value);

  String toString() {
    if (ApiType.DYNAMIC == type) {
      return 'const $name = $value';
    }
    return 'const $type $name = $value';
  }
}

class FinalVar extends AbstractVar {
  FinalVar(String name,
      {ApiType type: ApiType.DYNAMIC, String value: 'null'})
      : super(name, type: type, value: value);

  String toString() {
    if (ApiType.DYNAMIC == type) {
      return 'final $name = $value';
    }
    return 'final $type $name = $value';
  }
}

class NamedArg extends ConstVar {
  NamedArg(String name,
      {ApiType type: ApiType.DYNAMIC, String value: 'null'})
      : super(name, type: type, value: value);

  String toString() {
    if (ApiType.DYNAMIC == type) {
      return '$name: $value';
    }
    return '$type $name: $value';
  }
}

class PositionalArg extends ConstVar {
  PositionalArg(String name,
      {ApiType type: ApiType.DYNAMIC, String value: 'null'})
      : super(name, type: type, value: value);

  String toString() {
    if (ApiType.DYNAMIC == type) {
      return '$name = $value';
    }
    return '$type $name = $value';
  }
}

class Var extends AbstractVar {
  Var(String name,
      {ApiType type: ApiType.DYNAMIC, String value: 'null'})
      : super(name, type: type, value: value);

  String toString() {
    if (ApiType.DYNAMIC == type) {
      return 'var $name = $value';
    }
    return '$type $name = $value';
  }
}
