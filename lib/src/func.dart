// Copyright (c) 2016, Era Productions. All rights reserved.

library apigen.src.api_variable;

import 'var.dart';
import 'base.dart';

class Func extends TypedApiElement {
  final List<Arg> arguments;
  final String body;
  final OptionalArgumentsDeclaration optionalArguments;

  const Func(String name,
      {this.arguments: const <Arg>[],
      this.body: '',
      this.optionalArguments,
      ApiType type: ApiType.DYNAMIC})
      : super(name, type);

  String toString() {
    final buf = new StringBuffer();
    if (ApiType.DYNAMIC != type) {
      buf.write('$type ');
    }
    buf.write('$name(');
    if (arguments.isNotEmpty) {
      buf.write(arguments.join(', '));
    }
    if (null != optionalArguments && optionalArguments.arguments.isNotEmpty) {
      if (arguments.isNotEmpty) {
        buf.write(', ');
      }
      buf.write('${optionalArguments}');
    }
    buf.write(') ');
    if (body.isEmpty) {
      buf.write('{}');
    } else {
      buf.write('{\n  $body\n}');
    }
    return buf.toString();
  }
}

class NamedArgumentsDeclaration
    extends OptionalArgumentsDeclaration<NamedArg> {
  const NamedArgumentsDeclaration(List<NamedArg> args) : super(args);

  String toString() => '{${super.toString()}}';
}

abstract class OptionalArgumentsDeclaration<T extends ConstVar> {
  final List<T> arguments;

  const OptionalArgumentsDeclaration(this.arguments);

  String toString() => arguments.join(', ');
}

class PositionalArgumentsDeclaration
    extends OptionalArgumentsDeclaration<PositionalArg> {
  const PositionalArgumentsDeclaration(List<PositionalArg> args)
      : super(args);

  String toString() => '[${super.toString()}]';
}
