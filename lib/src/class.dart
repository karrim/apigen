// Copyright (c) 2016, Era Productions. All rights reserved.

library apigen.src.api_class;

import 'func.dart';
import 'var.dart';
import 'base.dart';

class Class extends TypedApiElement {
  final bool isAbstract;

  final List<Func> constructors;

  final List<Func> functions;

  final List<ApiType> parameterizedTypes;

  final List<Func> staticFunctions;

  final List<Var> staticVariables;

  final List<Var> variables;

  const Class(String name,
      {this.isAbstract: false,
      this.constructors: const <Func>[],
      this.functions: const <Func>[],
      this.parameterizedTypes: const <ApiType>[],
      this.staticFunctions: const <Func>[],
      this.staticVariables: const <Var>[],
      this.variables: const <Var>[]})
      : super(name, null);

  ApiType get type => new ApiType(name, parameterizedTypes);

  String toString() {
    final buf = new StringBuffer();
    if (isAbstract) {
      buf.write('abstract ');
    }
    buf.write('class $name');
    if (parameterizedTypes.isNotEmpty) {
      buf.write('<${parameterizedTypes.join(', ')}>');
    }
    buf.write(' {');
    if (constructors.isEmpty &&
        functions.isEmpty &&
        staticFunctions.isEmpty &&
        staticVariables.isEmpty &&
        variables.isEmpty) {
      buf.write('}');
      return buf.toString();
    }
    for (var v in staticVariables) {
      buf.write('\n\n  static $v;');
    }
    for (var v in variables) {
      buf.write('\n\n  $v;');
    }
    for (var c in constructors) {
      buf.write('\n\n  $name');
      if (c.name.isNotEmpty) {
        buf.write('.');
      }
      buf.write('${c}');
    }
    for (var f in functions) {
      buf.write('\n\n  $f');
    }
    for (var f in staticFunctions) {
      buf.write('\n\n  static $f');
    }
    buf.write('\n}');
    return buf.toString();
  }
}
